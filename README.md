# NDD Check4J

NDD Check4J provides easy parameters checking with a fluent or a concise API.

The documentation of the master branch is [available on GitLab Pages][ndd-check4j-gitlab-pages].

The documentations of the other branches are [available in the GitLab Environments section][ndd-check4j-gitlab-environments].
The environments of these branches are accessible in one of the `Available` or `Stopped` tabs then by clicking on the `Open` button at the right.

## Usage

Before NDD Check4J, aka the messy version:

```java
public void checkMyParametersWithoutCheck4J(final String iJustCantBeEmpty, String meNeither) {
    if (iJustCantBeEmpty == null || iJustCantBeEmpty.length() == 0) {
        throw new IllegalArgumentException("iJustCantBeEmpty just can't be empty, told you so!");
    }
    if (meNeither == null || meNeither.length() == 0) {
        throw new IllegalArgumentException("meNeither neither by the way");
    }
    this.iJustCantBeEmpty = iJustCantBeEmpty;
    this.meNeither = meNeither;
}
```

With the **fluent version** of NDD Check4J (see the [documentation][ndd-check4j-documentation-fluent]):

```java
import static name.didier.david.check4j.FluentCheckers.checkThat;

public void checkMyParametersWithCheck4J(final String iJustCantBeEmpty) {
    this.iJustCantBeEmpty = checkThat(iJustCantBeEmpty).isNotEmpty().thenAssign();
    this.meNeither        = checkThat(meNeither).as("meNeither").isNotEmpty().thenAssign();
}
```

`checkThat()` creates a new `Checker` upon which you can chain any validation method such as `isNotEmpty()`. If you want the message to reference the parameter name, you must use `as()` since there is no other way to get it. If you want to assign the parameter, which is a quite common thing, you must call `thenAssign()` at the end of the chain, effectively terminating it.


If this is too verbose for you, there is a **concise version** of NDD Check4J (see the [documentation][ndd-check4j-documentation-concise]):

```java
public void checkMyParametersWithCheck4JButBeConcisePlease(final String iJustCantBeEmpty) {
    this.iJustCantBeEmpty = checkNotEmpty(iJustCantBeEmpty);
    this.meNeither = checkNotEmpty(meNeither, "meNeither");
}
```

For the time being, only very basic checks are provided, but they should cover at least 80% of real life use cases and other may be added...

## Setup

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david</groupId>
  <artifactId>ndd-check4j</artifactId>
  <version>X.Y.Z</version>
</dependency>
```

## Reports

Available reports:

- [Maven reports][ndd-check4j-maven-reports] including CheckStyle, DependencyCheck, JaCoCo, PMD and SpotBugs;
- [Sonar reports][ndd-check4j-sonar-reports];

## About

Thanks to the [AssertJ] contributors which inspired this library.

Copyright Ⓒ David DIDIER 2014-2022

<!-- =============================================================================================================== -->

[ndd-check4j-documentation-concise]: https://ndd-oss.gitlab.io/java/ndd-check4j/concise-checkers.html
[ndd-check4j-documentation-fluent]: https://ndd-oss.gitlab.io/java/ndd-check4j/fluent-checkers.html
[ndd-check4j-gitlab-environments]: https://gitlab.com/ndd-oss/java/ndd-check4j/-/environments
[ndd-check4j-gitlab-pages]: https://ndd-oss.gitlab.io/java/ndd-check4j/
[ndd-check4j-maven-reports]: https://ndd-oss.gitlab.io/java/ndd-check4j/project-reports.html
[ndd-check4j-sonar-reports]: https://sonarcloud.io/project/overview?id=ndd-oss:java:ndd-maven
[AssertJ]: https://assertj.github.io/doc/
