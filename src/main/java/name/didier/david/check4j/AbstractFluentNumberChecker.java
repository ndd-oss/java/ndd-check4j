package name.didier.david.check4j;

/**
 * Common checking methods for {@link Number}s.
 *
 * @param <C> the "self" type of this checker class.
 * @param <A> the type of the "actual" value.
 */
public abstract class AbstractFluentNumberChecker<C extends AbstractFluentNumberChecker<C, A>, A extends Number>
        extends AbstractFluentChecker<C, A> {

    /**
     * Abstract constructor.
     *
     * @param actual   the actual value to check.
     * @param selfType {@code this} checker object.
     */
    protected AbstractFluentNumberChecker(final A actual, final Class<?> selfType) {
        super(actual, selfType);
    }

    /**
     * Verifies that the actual value is positive.
     *
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not positive.
     */
    public C isPositive() {
        return isGreaterThan(zero());
    }

    /**
     * Verifies that the actual value is strictly positive.
     *
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not strictly positive.
     */
    public C isStrictlyPositive() {
        return isStrictlyGreaterThan(zero());
    }

    /**
     * Verifies that the actual value is negative.
     *
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not negative.
     */
    public C isNegative() {
        return isLessThan(zero());
    }

    /**
     * Verifies that the actual value is strictly negative.
     *
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not strictly negative.
     */
    public C isStrictlyNegative() {
        return isStrictlyLessThan(zero());
    }

    /**
     * Verifies that the actual value is less than the specified one.
     *
     * @param other the value to compare against.
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not less than the specified one.
     */
    public C isLessThan(final Number other) {
        isNotNull();
        if (actual.doubleValue() > other.doubleValue()) {
            throw expectedParameterTo("be less than <%s> but was <%s>", ts(other), ts(actual));
        }
        return myself;
    }

    /**
     * Verifies that the actual value is strictly less than the specified one.
     *
     * @param other the value to compare against.
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not strictly less than the specified one.
     */
    public C isStrictlyLessThan(final Number other) {
        isNotNull();
        if (actual.doubleValue() >= other.doubleValue()) {
            throw expectedParameterTo("be strictly less than <%s> but was <%s>", ts(other), ts(actual));
        }
        return myself;
    }

    /**
     * Verifies that the actual value is greater than the specified one.
     *
     * @param other the value to compare against.
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not greater than the specified one.
     */
    public C isGreaterThan(final Number other) {
        isNotNull();
        if (actual.doubleValue() < other.doubleValue()) {
            throw expectedParameterTo("be greater than <%s> but was <%s>", ts(other), ts(actual));
        }
        return myself;
    }

    /**
     * Verifies that the actual value is strictly greater than the specified one.
     *
     * @param other the value to compare against.
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not strictly greater than the specified
     *                                  one.
     */
    public C isStrictlyGreaterThan(final Number other) {
        isNotNull();
        if (actual.doubleValue() <= other.doubleValue()) {
            throw expectedParameterTo("be strictly greater than <%s> but was <%s>", ts(other), ts(actual));
        }
        return myself;
    }

    /**
     * Verifies that the actual value is between than the specified ones.
     *
     * @param minimum the minimum value to compare against.
     * @param maximum the maximum value to compare against.
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not between than the specified ones.
     */
    public C isBetween(final Number minimum, final Number maximum) {
        return isNotNull().isGreaterThan(minimum).isLessThan(maximum);
    }

    /**
     * Verifies that the actual value is strictly between than the specified ones.
     *
     * @param minimum the minimum value to compare against.
     * @param maximum the maximum value to compare against.
     * @return this checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or not strictly between than the specified
     *                                  ones.
     */
    public C isStrictlyBetween(final Number minimum, final Number maximum) {
        return isNotNull().isStrictlyGreaterThan(minimum).isStrictlyLessThan(maximum);
    }

    /**
     * Returns a string representation of the specified number.
     *
     * @param number the number to represent.
     * @return a string representation of the specified number.
     */
    protected String ts(final Number number) {
        return number.toString();
    }

    /**
     * Returns the number zero.
     *
     * @return the number zero.
     */
    protected abstract A zero();
}
