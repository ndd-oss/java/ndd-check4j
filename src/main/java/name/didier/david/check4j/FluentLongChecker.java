package name.didier.david.check4j;

/**
 * Checking methods for {@link Long}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(Long)}</code>.
 */
public class FluentLongChecker
        extends AbstractFluentNumberChecker<FluentLongChecker, Long> {

    /**
     * Abstract constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentLongChecker(final Long actual) {
        super(actual, FluentLongChecker.class);
    }

    @Override
    protected Long zero() {
        return 0L;
    }
}
