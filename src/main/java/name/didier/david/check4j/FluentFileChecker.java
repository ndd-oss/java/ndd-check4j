package name.didier.david.check4j;

import java.io.File;

/**
 * Checking methods for {@link File}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(File)}</code>.
 */
public class FluentFileChecker
        extends AbstractFluentChecker<FluentFileChecker, File> {

    /**
     * Hidden constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentFileChecker(final File actual) {
        super(actual, FluentFileChecker.class);
    }

    /**
     * Verifies that the actual file is not {@code null} and exist.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual file is {@code null} or does not exist.
     */
    public FluentFileChecker exists() {
        isNotNull();
        if (!actual.exists()) {
            throw expectedParameterTo("be an existing file");
        }
        return myself;
    }

    /**
     * Verifies that the actual file is not {@code null} and is an existing regular file.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual file is {@code null} or is not an existing regular file.
     */
    public FluentFileChecker isFile() {
        isNotNull();
        if (!actual.isFile()) {
            throw expectedParameterTo("be an existing regular file");
        }
        return myself;
    }

    /**
     * Verifies that the actual file is not {@code null} and is an existing regular directory.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual file is {@code null} or is not an existing regular directory.
     */
    public FluentFileChecker isDirectory() {
        isNotNull();
        if (!actual.isDirectory()) {
            throw expectedParameterTo("be an existing regular directory");
        }
        return myself;
    }
}
