package name.didier.david.check4j;

/**
 * Checking methods for {@link Float}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(Float)}</code>.
 */
public class FluentFloatChecker
        extends AbstractFluentNumberChecker<FluentFloatChecker, Float> {

    /**
     * Abstract constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentFloatChecker(final Float actual) {
        super(actual, FluentFloatChecker.class);
    }

    @Override
    protected Float zero() {
        return (float) 0;
    }
}
