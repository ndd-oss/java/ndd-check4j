package name.didier.david.check4j;

import java.util.Collection;

/**
 * Checking methods for array of {@link Collection}s. To create a new instance of this class, invoke
 * <code>{@link FluentCheckers#checkThat(Collection)}</code>.
 *
 * @param <C> the type of the "actual" collection.
 * @param <T> the type of the "actual" value.
 */
public class FluentCollectionChecker<C extends Collection<T>, T>
        extends AbstractFluentChecker<FluentCollectionChecker<C, T>, C> {

    /**
     * Hidden constructor.
     *
     * @param actual the actual value to check.
     */
    protected FluentCollectionChecker(final C actual) {
        super(actual, FluentCollectionChecker.class);
    }

    /**
     * Verifies that the actual value is not {@code null} nor empty.
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual value is {@code null} or empty.
     */
    public FluentCollectionChecker<C, T> isNotEmpty() {
        isNotNull();
        if (actual.isEmpty()) {
            throw expectedParameterTo("not be empty");
        }
        return myself;
    }
}
