package name.didier.david.check4j;

/**
 * Common checking methods for {@link CharSequence}s.
 *
 * @param <A> the type of the "actual" value.
 * @param <C> the "self" type of this checker class.
 */
public abstract class AbstractFluentCharSequenceChecker<C extends AbstractFluentCharSequenceChecker<C, A>, A extends CharSequence>
        extends AbstractFluentChecker<C, A> {

    /**
     * Abstract constructor.
     *
     * @param actual   the actual value to check.
     * @param selfType {@code this} checker object.
     */
    protected AbstractFluentCharSequenceChecker(final A actual, final Class<?> selfType) {
        super(actual, selfType);
    }

    /**
     * Verifies that the actual {@code CharSequence} is not empty, i.e., is not {@code null} and has a length of 1 or
     * more (including any leading or trailing whitespace).
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual {@code CharSequence} is empty (has a length of 0).
     */
    public C isNotEmpty() {
        isNotNull();
        if (actual.length() == 0) {
            throw expectedParameterTo("not be empty", actual);
        }
        return myself;
    }

    /**
     * Verifies that the actual {@code CharSequence} is not blank, i.e., is not {@code null} and has a length of 1 or
     * more (omitting any leading and trailing whitespace).
     *
     * @return {@code this} checker object.
     * @throws IllegalArgumentException if the actual {@code CharSequence} is blank.
     */
    public abstract FluentStringChecker isNotBlank();
}
