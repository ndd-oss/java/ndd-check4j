package name.didier.david.check4j;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;

/**
 * Entry point for checking methods for different data types. Each method in this class is a static factory for the
 * type-specific checker objects. The purpose of this class is to make code more readable. For example:
 *
 * <pre>
 * {@link FluentCheckers#checkThat(Object) checkThat}(param1).{@link FluentObjectChecker#isNotNull isNotNull}();
 * </pre>
 *
 * @see ConciseCheckers
 */
public class FluentCheckers {

    /**
     * Default constructor.
     */
    protected FluentCheckers() {
        super();
    }

    /**
     * Creates a new instance of {@link FluentObjectChecker}.
     *
     * @param <T>    the type of the actual value.
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static <T> FluentObjectChecker<T> checkThat(final T actual) {
        return new FluentObjectChecker<>(actual);
    }

    /**
     * Creates a new instance of {@link FluentStringChecker}.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentStringChecker checkThat(final String actual) {
        return new FluentStringChecker(actual);
    }

    /**
     * Creates a new instance of {@link FluentIntegerChecker}.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentIntegerChecker checkThat(final Integer actual) {
        return new FluentIntegerChecker(actual);
    }

    /**
     * Creates a new instance of {@link FluentLongChecker}.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentLongChecker checkThat(final Long actual) {
        return new FluentLongChecker(actual);
    }

    /**
     * Creates a new instance of {@link FluentFloatChecker}.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentFloatChecker checkThat(final Float actual) {
        return new FluentFloatChecker(actual);
    }

    /**
     * Creates a new instance of {@link FluentDoubleChecker}.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentDoubleChecker checkThat(final Double actual) {
        return new FluentDoubleChecker(actual);
    }

    /**
     * Creates a new instance of {@link FluentObjectArrayChecker}.
     *
     * @param <T>    the type of the actual value.
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static <T> FluentObjectArrayChecker<T> checkThat(final T[] actual) {
        return new FluentObjectArrayChecker<>(actual);
    }

    /**
     * Creates a new instance of {@link FluentIterableChecker}.
     *
     * @param <C>    the type of the actual iterable.
     * @param <T>    the type of the actual value.
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static <C extends Iterable<T>, T> FluentIterableChecker<C, T> checkThat(final C actual) {
        return new FluentIterableChecker<>(actual);
    }

    /**
     * Creates a new instance of {@link FluentCollectionChecker}.
     *
     * @param <C>    the type of the actual collection.
     * @param <T>    the type of the actual value.
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static <C extends Collection<T>, T> FluentCollectionChecker<C, T> checkThat(final C actual) {
        return new FluentCollectionChecker<>(actual);
    }

    /**
     * Creates a new instance of {@link FluentFileChecker}.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentFileChecker checkThat(final File actual) {
        return new FluentFileChecker(actual);
    }

    /**
     * Creates a new instance of {@link FluentPathChecker}.
     *
     * @param actual the actual value.
     * @return the created checker object.
     */
    public static FluentPathChecker checkThat(final Path actual) {
        return new FluentPathChecker(actual);
    }
}
