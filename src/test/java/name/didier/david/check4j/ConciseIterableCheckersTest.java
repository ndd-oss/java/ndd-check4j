package name.didier.david.check4j;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static name.didier.david.check4j.ConciseCheckers.checkNotEmpty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

class ConciseIterableCheckersTest
        extends AbstractConciseCheckersTestBase {

    private static final Iterable<Object> NULL = null;
    private static final Iterable<Object> EMPTY = newArrayList();
    private static final Iterable<Object> NOT_EMPTY = newArrayList(new Object());

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkNotEmpty_should_pass_if_not_empty_then_return_parameter() {
        assertThat(checkNotEmpty(NOT_EMPTY)).isSameAs(NOT_EMPTY);
    }

    @Test
    void checkNotEmpty_should_pass_if_not_empty_then_return_parameter_with_pName() {
        assertThat(checkNotEmpty(NOT_EMPTY, P_NAME)).isSameAs(NOT_EMPTY);
    }

    @Test
    void checkNotEmpty_result_should_not_be_casted() {
        List<String> stringList = newArrayList("some string");
        List<String> uncastedStringList = checkNotEmpty(stringList);
        assertThat(uncastedStringList).isSameAs(stringList);

        Set<String> stringSet = newHashSet("some string");
        Set<String> uncastedStringSet = checkNotEmpty(stringSet);
        assertThat(uncastedStringSet).isSameAs(stringSet);
    }

    @Test
    void checkNotEmpty_should_fail_if_null() {
        try {
            checkNotEmpty(NULL);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    @Test
    void checkNotEmpty_should_fail_if_null_with_pName() {
        try {
            checkNotEmpty(NULL, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    void checkNotEmpty_should_fail_if_empty() {
        try {
            checkNotEmpty(EMPTY);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to not be empty");
        }
    }

    @Test
    void checkNotEmpty_should_fail_if_empty_with_pName() {
        try {
            checkNotEmpty(EMPTY, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to not be empty");
        }
    }

}
