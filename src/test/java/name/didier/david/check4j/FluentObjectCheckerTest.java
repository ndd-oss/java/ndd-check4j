package name.didier.david.check4j;

class FluentObjectCheckerTest
        extends AbstractFluentCheckerTestBase<FluentObjectChecker<Object>, Object> {

    @Override
    protected Object newActual() {
        return new Object();
    }

    @Override
    protected FluentObjectChecker<Object> newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected FluentObjectChecker<Object> newChecker(final Object actual) {
        return new FluentObjectChecker<>(actual);
    }

    @Override
    protected Class<?> getActualOtherClass() {
        return Object.class;
    }
}
