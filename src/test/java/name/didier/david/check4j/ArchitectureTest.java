package name.didier.david.check4j;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING;
import static com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME;
import static org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.DisplayNameGeneration;

@DisplayNameGeneration(ReplaceUnderscores.class)
class ArchitectureTest {

    abstract static class AbstractArchitectureTestCase {

        @ArchTest
        static final ArchRule DO_NOT_ACCESS_STANDARD_STREAMS = NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS;
        @ArchTest
        static final ArchRule DO_NOT_THROW_GENERIC_EXCEPTIONS = NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS;
        @ArchTest
        static final ArchRule DO_NOT_USE_JAVA_UTIL_LOGGING = NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING;
        @ArchTest
        static final ArchRule DO_NOT_USE_JODA_TIME = NO_CLASSES_SHOULD_USE_JODATIME;
    }

    @AnalyzeClasses(packages = "name.didier.david.check4j", importOptions = ImportOption.DoNotIncludeTests.class)
    static class MainArchitectureTest
            extends AbstractArchitectureTestCase {

        @ArchTest
        static final ArchRule DO_NOT_USE_ANY_EXTERNAL_LIBRARY = classes().that()
                .resideInAPackage("name.didier.david.check4j..")
                .should().onlyDependOnClassesThat().resideInAnyPackage(
                        "name.didier.david.check4j..",
                        "java..");
    }

    @AnalyzeClasses(packages = "name.didier.david.check4j", importOptions = ImportOption.OnlyIncludeTests.class)
    static class TestArchitectureTest
            extends AbstractArchitectureTestCase {

        @ArchTest
        static final ArchRule DO_NOT_USE_JUNIT_4 = noClasses().that()
                .resideInAPackage("name.didier.david.check4j..")
                .should().dependOnClassesThat().haveFullyQualifiedName("org.junit.Test");
    }
}
