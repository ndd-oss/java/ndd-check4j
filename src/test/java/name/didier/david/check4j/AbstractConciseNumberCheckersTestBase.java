package name.didier.david.check4j;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_STRICTLY_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_STRICTLY_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_STRICTLY_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_STRICTLY_LESS;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

abstract class AbstractConciseNumberCheckersTestBase<N extends Number>
        extends AbstractConciseCheckersTestBase {

    N negative;
    N zero;
    N positive;

    @BeforeEach
    void setUp() {
        negative = numberN();
        zero = numberZ();
        positive = numberP();
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkNegative_should_pass_if_strictly_negative_then_return_parameter() {
        assertThat(checkNegative(negative)).isEqualTo(negative);
        assertThat(checkNegative(negative, P_NAME)).isEqualTo(negative);
    }

    @Test
    void checkNegative_should_pass_if_zero() {
        assertThat(checkNegative(zero)).isEqualTo(zero);
        assertThat(checkNegative(zero, P_NAME)).isEqualTo(zero);
    }

    @Test
    void checkNegative_should_fail_if_strictly_positive() {
        try {
            checkNegative(positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            checkNegative(positive, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkStrictlyNegative_should_pass_if_strictly_negative_then_return_parameter() {
        assertThat(checkStrictlyNegative(negative)).isEqualTo(negative);
        assertThat(checkStrictlyNegative(negative, P_NAME)).isEqualTo(negative);
    }

    @Test
    void checkStrictlyNegative_should_fail_if_zero() {
        try {
            checkStrictlyNegative(zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        try {
            checkStrictlyNegative(zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    void checkStrictlyNegative_should_fail_if_strictly_positive() {
        try {
            checkStrictlyNegative(positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            checkStrictlyNegative(positive, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkPositive_should_pass_if_strictly_positive_then_return_parameter() {
        assertThat(checkPositive(positive)).isEqualTo(positive);
        assertThat(checkPositive(positive, P_NAME)).isEqualTo(positive);
    }

    @Test
    void checkPositive_should_pass_if_zero() {
        assertThat(checkPositive(zero)).isEqualTo(zero);
        assertThat(checkPositive(zero, P_NAME)).isEqualTo(zero);
    }

    @Test
    void checkPositive_should_fail_if_strictly_negative() {
        try {
            checkPositive(negative);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            checkPositive(negative, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkStrictlyPositive_should_pass_if_strictly_positive_then_return_parameter() {
        assertThat(checkStrictlyPositive(positive)).isEqualTo(positive);
        assertThat(checkStrictlyPositive(positive, P_NAME)).isEqualTo(positive);
    }

    @Test
    void checkStrictlyPositive_should_fail_if_zero() {
        try {
            checkStrictlyPositive(zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        try {
            checkStrictlyPositive(zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    void checkStrictlyPositive_should_fail_if_strictly_negative() {
        try {
            checkStrictlyPositive(negative);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            checkStrictlyPositive(negative, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkLessThan_should_pass_if_strictly_less_than_then_return_parameter() {
        assertThat(checkLessThan(negative, zero)).isEqualTo(negative);
        assertThat(checkLessThan(negative, zero, P_NAME)).isEqualTo(negative);
    }

    @Test
    void checkLessThan_should_pass_if_zero() {
        assertThat(checkLessThan(zero, zero)).isEqualTo(zero);
        assertThat(checkLessThan(zero, zero, P_NAME)).isEqualTo(zero);
    }

    @Test
    void checkLessThan_should_fail_if_strictly_greater() {
        try {
            checkLessThan(positive, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            checkLessThan(positive, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkStrictlyLessThan_should_pass_if_strictly_less_than_then_return_parameter() {
        assertThat(checkStrictlyLessThan(negative, zero)).isEqualTo(negative);
        assertThat(checkStrictlyLessThan(negative, zero, P_NAME)).isEqualTo(negative);
    }

    @Test
    void checkStrictlyLessThan_should_fail_if_zero() {
        try {
            checkStrictlyLessThan(zero, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        try {
            checkStrictlyLessThan(zero, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    void checkStrictlyLessThan_should_fail_if_strictly_greater() {
        try {
            checkStrictlyLessThan(positive, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            checkStrictlyLessThan(positive, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkGreaterThan_should_pass_if_strictly_greater_than_then_return_parameter() {
        assertThat(checkGreaterThan(positive, zero)).isEqualTo(positive);
        assertThat(checkGreaterThan(positive, zero, P_NAME)).isEqualTo(positive);
    }

    @Test
    void checkGreaterThan_should_pass_if_zero() {
        assertThat(checkGreaterThan(zero, zero)).isEqualTo(zero);
        assertThat(checkGreaterThan(zero, zero, P_NAME)).isEqualTo(zero);
    }

    @Test
    void checkGreaterThan_should_fail_if_strictly_less() {
        try {
            checkGreaterThan(negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            checkGreaterThan(negative, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkStrictlyGreaterThan_should_pass_if_strictly_greater_than_then_return_parameter() {
        assertThat(checkStrictlyGreaterThan(positive, zero)).isEqualTo(positive);
        assertThat(checkStrictlyGreaterThan(positive, zero, P_NAME)).isEqualTo(positive);
    }

    @Test
    void checkStrictlyGreaterThan_should_fail_if_zero() {
        try {
            checkStrictlyGreaterThan(zero, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        try {
            checkStrictlyGreaterThan(zero, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    void checkStrictlyGreaterThan_should_fail_if_strictly_less() {
        try {
            checkStrictlyGreaterThan(negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            checkStrictlyGreaterThan(negative, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkBetween_should_pass_if_strictly_between_then_return_parameter() {
        assertThat(checkBetween(zero, negative, positive)).isEqualTo(zero);
        assertThat(checkBetween(zero, negative, positive, P_NAME)).isEqualTo(zero);
    }

    @Test
    void checkBetween_should_pass_if_equals_to_minimum_then_return_parameter() {
        assertThat(checkBetween(negative, negative, positive)).isEqualTo(negative);
        assertThat(checkBetween(negative, negative, positive, P_NAME)).isEqualTo(negative);
    }

    @Test
    void checkBetween_should_pass_if_equals_to_maximum_then_return_parameter() {
        assertThat(checkBetween(positive, negative, positive)).isEqualTo(positive);
        assertThat(checkBetween(positive, negative, positive, P_NAME)).isEqualTo(positive);
    }

    @Test
    void checkBetweenThan_should_fail_if_strictly_less() {
        try {
            checkBetween(negative, zero, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        try {
            checkBetween(negative, zero, positive, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    void checkBetweenThan_should_fail_if_strictly_greater() {
        try {
            checkBetween(positive, negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        try {
            checkBetween(positive, negative, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void checkStrictlyBetween_should_pass_if_strictly_between_then_return_parameter() {
        assertThat(checkStrictlyBetween(zero, negative, positive)).isEqualTo(zero);
        assertThat(checkStrictlyBetween(zero, negative, positive, P_NAME)).isEqualTo(zero);
    }

    @Test
    void checkStrictlyBetween_should_fail_if_equals_to_minimum_then_return_parameter() {
        try {
            checkStrictlyBetween(zero, zero, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        try {
            checkStrictlyBetween(zero, zero, positive, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    void checkStrictlyBetween_should_fail_if_equals_to_maximum_then_return_parameter() {
        try {
            checkStrictlyBetween(zero, negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        try {
            checkStrictlyBetween(zero, negative, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    void checkStrictlyBetweenThan_should_fail_if_strictly_less() {
        try {
            checkStrictlyBetween(negative, zero, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        try {
            checkStrictlyBetween(negative, zero, positive, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    void checkStrictlyBetweenThan_should_fail_if_strictly_greater() {
        try {
            checkStrictlyBetween(positive, negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        try {
            checkStrictlyBetween(positive, negative, zero, P_NAME);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract N checkNegative(N numberN);

    protected abstract N checkNegative(N numberN, String parameterName);

    protected abstract N checkStrictlyNegative(N numberN);

    protected abstract N checkStrictlyNegative(N numberN, String parameterName);

    protected abstract N checkPositive(N numberN);

    protected abstract N checkPositive(N numberN, String parameterName);

    protected abstract N checkStrictlyPositive(N numberN);

    protected abstract N checkStrictlyPositive(N numberN, String parameterName);

    protected abstract N checkLessThan(N numberN, Number max);

    protected abstract N checkLessThan(N numberN, Number max, String parameterName);

    protected abstract N checkStrictlyLessThan(N numberN, Number max);

    protected abstract N checkStrictlyLessThan(N numberN, Number max, String parameterName);

    protected abstract N checkGreaterThan(N numberN, Number min);

    protected abstract N checkGreaterThan(N numberN, Number min, String parameterName);

    protected abstract N checkStrictlyGreaterThan(N numberN, Number min);

    protected abstract N checkStrictlyGreaterThan(N numberN, Number min, String parameterName);

    protected abstract N checkBetween(N numberN, Number min, Number max);

    protected abstract N checkBetween(N numberN, Number min, Number max, String parameterName);

    protected abstract N checkStrictlyBetween(N numberN, Number min, Number max);

    protected abstract N checkStrictlyBetween(N numberN, Number min, Number max, String parameterName);

    /** Negative as number. */
    protected abstract N numberN();

    /** Zero as number. */
    protected abstract N numberZ();

    /** Positive as number. */
    protected abstract N numberP();

    /** Negative as string. */
    protected String stringN() {
        return numberN().toString();
    }

    /** Zero as string. */
    protected String stringZ() {
        return numberZ().toString();
    }

    /** Positive as string. */
    protected String stringP() {
        return numberP().toString();
    }
}
