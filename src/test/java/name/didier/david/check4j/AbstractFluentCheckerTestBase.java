package name.didier.david.check4j;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import org.junit.jupiter.api.Test;

abstract class AbstractFluentCheckerTestBase<C extends AbstractFluentChecker<C, A>, A> {

    protected static final String P_NAME = "P";

    protected static final String NULL_MESSAGE = "Expected parameter to not be null";
    protected static final String NULL_P_MESSAGE = "Expected parameter 'P' to not be null";

    private static final String STRING_FORMAT = "be some %s";
    private static final String STRING_ARG = "STRING_ARG";

    @Test
    void as_should_return_self() {
        AbstractFluentChecker<C, A> checker = newChecker();
        assertThat(checker.as(P_NAME)).isSameAs(checker);
    }

    @Test
    void as_should_set_the_parameter_name() {
        assertThat(newChecker().as(P_NAME).getName()).isEqualTo(P_NAME);
    }

    @Test
    void name_should_be_null_by_default() {
        assertThat(newChecker().getName()).isNull();
    }

    @Test
    void thenAssign_should_return_the_actual_value() {
        A actual = newActual();
        assertThat(newChecker(actual).thenAssign()).isSameAs(actual);
    }

    @Test
    void getActual_should_return_the_actual_value() {
        A actual = newActual();
        assertThat(newChecker(actual).getActual()).isSameAs(actual);
    }

    @Test
    void expectedParameterTo_should_return_an_IAE_without_parameter_name() {
        IllegalArgumentException iae = newChecker().expectedParameterTo(STRING_FORMAT, STRING_ARG);
        assertThat(iae.getMessage()).isEqualTo("Expected parameter to be some STRING_ARG");
    }

    @Test
    void expectedParameterTo_should_return_an_IAE_with_empty_parameter_name() {
        IllegalArgumentException iae = newChecker().as("").expectedParameterTo(STRING_FORMAT, STRING_ARG);
        assertThat(iae.getMessage()).isEqualTo("Expected parameter to be some STRING_ARG");
    }

    @Test
    void expectedParameterTo_should_return_an_IAE_with_pName() {
        IllegalArgumentException iae = newChecker().as(P_NAME).expectedParameterTo(STRING_FORMAT, STRING_ARG);
        assertThat(iae.getMessage()).isEqualTo("Expected parameter 'P' to be some STRING_ARG");
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isNotNull_should_pass_if_not_null() {
        assertThatNoException().isThrownBy(
                () -> newChecker().isNotNull());
    }

    @Test
    void isNotNull_should_return_self() {
        AbstractFluentChecker<C, A> checker = newChecker();
        assertThat(checker.isNotNull()).isSameAs(checker);
    }

    @Test
    void isNotNull_should_fail_if_null() {
        AbstractFluentChecker<C, A> checker = newChecker(null);
        try {
            checker.isNotNull();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    @Test
    void isNotNull_should_fail_if_null_with_pName() {
        AbstractFluentChecker<C, A> checker = newChecker(null).as(P_NAME);
        try {
            checker.isNotNull();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isInstanceOf_should_pass_if_not_null_and_instance_of_class() {
        assertThatNoException().isThrownBy(
                () -> newChecker().isInstanceOf(getActualClass()));
    }

    @Test
    void isInstanceOf_should_pass_if_not_null_and_instance_of_superclass() {
        assertThatNoException().isThrownBy(
                () -> newChecker().isInstanceOf(Object.class));
    }

    @Test
    void isInstanceOf_should_return_self() {
        AbstractFluentChecker<C, A> checker = newChecker();
        assertThat(checker.isInstanceOf(getActualClass())).isSameAs(checker);
    }

    @Test
    void isInstanceOf_should_fail_if_null() {
        AbstractFluentChecker<C, A> checker = newChecker(null);
        try {
            checker.isInstanceOf(getActualClass());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    @Test
    void isInstanceOf_should_fail_if_null_with_pName() {
        AbstractFluentChecker<C, A> checker = newChecker(null).as(P_NAME);
        try {
            checker.isInstanceOf(getActualClass());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    void isInstanceOf_should_fail_if_not_instance_of() {
        if (Object.class.equals(getActualClass())) {
            return;
        }

        AbstractFluentChecker<C, A> checker = newChecker();
        try {
            checker.isInstanceOf(getActualOtherClass());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format("Expected parameter to be an instance of <%s>\nbut was instance of <%s>",
                    getActualOtherClass().getName(), getActualClass().getName()));
        }
    }

    @Test
    void isInstanceOf_should_fail_if_not_instance_of_with_pName() {
        if (Object.class.equals(getActualClass())) {
            return;
        }

        AbstractFluentChecker<C, A> checker = newChecker().as(P_NAME);
        try {
            checker.isInstanceOf(getActualOtherClass());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e)
                    .hasMessage(format("Expected parameter 'P' to be an instance of <%s>\nbut was instance of <%s>",
                            getActualOtherClass().getName(), getActualClass().getName()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract A newActual();

    protected abstract AbstractFluentChecker<C, A> newChecker();

    protected abstract AbstractFluentChecker<C, A> newChecker(A actual);

    // ------------------------------

    private Class<?> getActualClass() {
        return newActual().getClass();
    }

    protected Class<?> getActualOtherClass() {
        return String.class;
    }
}
