package name.didier.david.check4j;

class FluentLongCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentLongChecker, Long> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentLongChecker newChecker(final Long actual) {
        return new FluentLongChecker(actual);
    }

    @Override
    protected Long numberN() {
        return -1L;
    }

    @Override
    protected Long numberZ() {
        return 0L;
    }

    @Override
    protected Long numberP() {
        return 1L;
    }
}
