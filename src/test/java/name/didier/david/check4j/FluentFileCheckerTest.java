package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.io.File;

import org.junit.jupiter.api.Test;

class FluentFileCheckerTest
        extends AbstractFluentCheckerTestBase<FluentFileChecker, File> {

    private static final File NOT_EXISTING = new File("some/path/that/is/really/unlikely/to/exist");
    private static final File EXISTING_DIRECTORY = new File("src/test/resources");
    private static final File EXISTING_FILE = new File("src/test/resources/logback-test.xml");

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void exists_should_return_self() {
        FluentFileChecker checkerD = newChecker(EXISTING_DIRECTORY);
        assertThat(checkerD.exists()).isEqualTo(checkerD);

        FluentFileChecker checkerF = newChecker(EXISTING_FILE);
        assertThat(checkerF.exists()).isEqualTo(checkerF);
    }

    @Test
    void exists_should_pass_if_file_exists() {
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).exists());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).as(P_NAME).exists());

        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).exists());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).as(P_NAME).exists());
    }

    @Test
    void exists_should_fail_if_file_does_not_exist() {
        FluentFileChecker checker1 = newChecker(NOT_EXISTING);
        try {
            checker1.exists();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing file");
        }

        FluentFileChecker checker2 = newChecker(NOT_EXISTING).as(P_NAME);
        try {
            checker2.exists();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing file");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isFile_should_return_self() {
        FluentFileChecker checker = newChecker(EXISTING_FILE);
        assertThat(checker.isFile()).isEqualTo(checker);
    }

    @Test
    void isFile_should_pass_if_file_is_a_regular_file() {
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).isFile());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).as(P_NAME).isFile());
    }

    @Test
    void isFile_should_fail_if_file_is_not_a_regular_file() {
        FluentFileChecker checker1 = newChecker(NOT_EXISTING);
        try {
            checker1.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular file");
        }

        FluentFileChecker checker2 = newChecker(NOT_EXISTING).as(P_NAME);
        try {
            checker2.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular file");
        }

        FluentFileChecker checker3 = newChecker(EXISTING_DIRECTORY);
        try {
            checker3.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular file");
        }

        FluentFileChecker checker4 = newChecker(EXISTING_DIRECTORY).as(P_NAME);
        try {
            checker4.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular file");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isDirectory_should_return_self() {
        FluentFileChecker checker = newChecker(EXISTING_DIRECTORY);
        assertThat(checker.isDirectory()).isEqualTo(checker);
    }

    @Test
    void isDirectory_should_pass_if_file_is_a_regular_directory() {
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).isDirectory());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).as(P_NAME).isDirectory());
    }

    @Test
    void isDirectory_should_fail_if_file_is_not_a_regular_directory() {
        FluentFileChecker checker1 = newChecker(NOT_EXISTING);
        try {
            checker1.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular directory");
        }

        FluentFileChecker checker2 = newChecker(NOT_EXISTING).as(P_NAME);
        try {
            checker2.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular directory");
        }

        FluentFileChecker checker3 = newChecker(EXISTING_FILE);
        try {
            checker3.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular directory");
        }

        FluentFileChecker checker4 = newChecker(EXISTING_FILE).as(P_NAME);
        try {
            checker4.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular directory");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected File newActual() {
        return EXISTING_DIRECTORY;
    }

    @Override
    protected FluentFileChecker newChecker() {
        return new FluentFileChecker(newActual());
    }

    @Override
    protected FluentFileChecker newChecker(File actual) {
        return new FluentFileChecker(actual);
    }
}
