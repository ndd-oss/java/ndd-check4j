package name.didier.david.check4j;

import static java.lang.String.format;

import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_STRICTLY_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_P_STRICTLY_LESS;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_STRICTLY_GREATER;
import static name.didier.david.check4j.NumberCheckersUtils.EXPECTED_STRICTLY_LESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

abstract class AbstractFluentNumberCheckerTestBase<C extends AbstractFluentNumberChecker<C, A>, A extends Number>
        extends AbstractFluentCheckerTestBase<C, A> {

    A negative;
    A zero;
    A positive;

    @BeforeEach
    void setUp() {
        negative = numberN();
        zero = numberZ();
        positive = numberP();
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isNegative_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isNegative()).isEqualTo(checker);
    }

    @Test
    void isNegative_should_pass_if_strictly_negative() {
        assertThatNoException().isThrownBy(
                () -> newChecker(negative).isNegative());
        assertThatNoException().isThrownBy(
                () -> newChecker(negative).as(P_NAME).isNegative());
    }

    @Test
    void isNegative_should_pass_if_zero() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isNegative());
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isNegative());
    }

    @Test
    void isNegative_should_fail_if_strictly_positive() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(positive);
        try {
            checker.isNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(positive).as(P_NAME);
        try {
            checkerWithName.isNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isStrictlyNegative_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(negative);
        assertThat(checker.isStrictlyNegative()).isEqualTo(checker);
    }

    @Test
    void isStrictlyNegative_should_pass_if_strictly_negative() {
        assertThatNoException().isThrownBy(
                () -> newChecker(negative).isStrictlyNegative());
        assertThatNoException().isThrownBy(
                () -> newChecker(negative).as(P_NAME).isStrictlyNegative());
    }

    @Test
    void isStrictlyNegative_should_fail_if_zero() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    void isStrictlyNegative_should_fail_if_strictly_positive() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(positive);
        try {
            checker.isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(positive).as(P_NAME);
        try {
            checkerWithName.isStrictlyNegative();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isPositive_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isPositive()).isEqualTo(checker);
    }

    @Test
    void isPositive_should_pass_if_strictly_positive() {
        assertThatNoException().isThrownBy(
                () -> newChecker(positive).isPositive());
        assertThatNoException().isThrownBy(
                () -> newChecker(positive).as(P_NAME).isPositive());
    }

    @Test
    void isPositive_should_pass_if_zero() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isPositive());
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isPositive());
    }

    @Test
    void isPositive_should_fail_if_strictly_negative() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(negative);
        try {
            checker.isPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(negative).as(P_NAME);
        try {
            checkerWithName.isPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isStrictlyPositive_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(positive);
        assertThat(checker.isStrictlyPositive()).isEqualTo(checker);
    }

    @Test
    void isStrictlyPositive_should_pass_if_strictly_positive() {
        assertThatNoException().isThrownBy(
                () -> newChecker(positive).isStrictlyPositive());
        assertThatNoException().isThrownBy(
                () -> newChecker(positive).as(P_NAME).isStrictlyPositive());
    }

    @Test
    void isStrictlyPositive_should_fail_if_zero() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    void isStrictlyPositive_should_fail_if_strictly_negative() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(negative);
        try {
            checker.isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(negative).as(P_NAME);
        try {
            checkerWithName.isStrictlyPositive();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isLessThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isLessThan(positive)).isEqualTo(checker);
    }

    @Test
    void isLessThan_should_pass_if_strictly_less() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isLessThan(positive));
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isLessThan(positive));
    }

    @Test
    void isLessThan_should_pass_if_equals() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isLessThan(zero));
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isLessThan(zero));
    }

    @Test
    void isLessThan_should_fail_if_strictly_greater() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isLessThan(negative);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringN(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isLessThan(negative);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringN(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isStrictlyLessThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isStrictlyLessThan(positive)).isEqualTo(checker);
    }

    @Test
    void isStrictlyLessThan_should_pass_if_strictly_less() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isStrictlyLessThan(positive));
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isStrictlyLessThan(positive));
    }

    @Test
    void isStrictlyLessThan_should_fail_if_equals() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isStrictlyLessThan(zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isStrictlyLessThan(zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringZ()));
        }
    }

    @Test
    void isStrictlyLessThan_should_fail_if_strictly_greater() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isStrictlyLessThan(negative);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringN(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isStrictlyLessThan(negative);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringN(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isGreaterThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isGreaterThan(negative)).isEqualTo(checker);
    }

    @Test
    void isGreaterThan_should_pass_if_strictly_greater() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isGreaterThan(negative));
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isGreaterThan(negative));
    }

    @Test
    void isGreaterThan_should_pass_if_equals() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isGreaterThan(zero));
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isGreaterThan(zero));
    }

    @Test
    void isGreaterThan_should_fail_if_strictly_less() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isGreaterThan(positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringP(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isGreaterThan(positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringP(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isStrictlyGreaterThan_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isStrictlyGreaterThan(negative)).isEqualTo(checker);
    }

    @Test
    void isStrictlyGreaterThan_should_pass_if_strictly_greater() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isStrictlyGreaterThan(negative));
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isStrictlyGreaterThan(negative));
    }

    @Test
    void isStrictlyGreaterThan_should_fail_if_equals() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isStrictlyGreaterThan(zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isStrictlyGreaterThan(zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringZ()));
        }
    }

    @Test
    void isStrictlyGreaterThan_should_fail_if_strictly_less() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        try {
            checker.isStrictlyGreaterThan(positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringP(), stringZ()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(zero).as(P_NAME);
        try {
            checkerWithName.isStrictlyGreaterThan(positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringP(), stringZ()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isBetween_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isBetween(negative, positive)).isEqualTo(checker);
    }

    @Test
    void isBetween_should_pass_if_strictly_between() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isBetween(negative, positive));
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).as(P_NAME).isBetween(negative, positive));
    }

    @Test
    void isBetween_should_pass_if_equals_to_minimum() {
        assertThatNoException().isThrownBy(
                () -> newChecker(negative).isBetween(negative, positive));
        assertThatNoException().isThrownBy(
                () -> newChecker(negative).as(P_NAME).isBetween(negative, positive));
    }

    @Test
    void isBetween_should_pass_if_equals_to_maximum() {
        assertThatNoException().isThrownBy(
                () -> newChecker(positive).isBetween(negative, positive));
        assertThatNoException().isThrownBy(
                () -> newChecker(positive).as(P_NAME).isBetween(negative, positive));
    }

    @Test
    void isBetweenThan_should_fail_if_strictly_less() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(negative);
        try {
            checker.isBetween(zero, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_GREATER, stringZ(), stringN()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(negative).as(P_NAME);
        try {
            checkerWithName.isBetween(zero, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    void isBetweenThan_should_fail_if_strictly_greater() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(positive);
        try {
            checker.isBetween(negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_LESS, stringZ(), stringP()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(positive).as(P_NAME);
        try {
            checkerWithName.isBetween(negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isStrictlyBetween_should_return_self() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(zero);
        assertThat(checker.isStrictlyBetween(negative, positive)).isEqualTo(checker);
    }

    @Test
    void isStrictlyBetween_should_pass_if_strictly_between() {
        assertThatNoException().isThrownBy(
                () -> newChecker(zero).isStrictlyBetween(negative, positive));
        assertThatNoException()
                .isThrownBy(() -> newChecker(zero).as(P_NAME).isStrictlyBetween(negative, positive));
    }

    @Test
    void isStrictlyBetween_should_fail_if_equals_to_minimum() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(negative);
        try {
            checker.isStrictlyBetween(negative, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringN(), stringN()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(negative).as(P_NAME);
        try {
            checkerWithName.isStrictlyBetween(negative, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringN(), stringN()));
        }
    }

    @Test
    void isStrictlyBetween_should_fail_if_equals_to_maximum() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(positive);
        try {
            checker.isStrictlyBetween(negative, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringP(), stringP()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(positive).as(P_NAME);
        try {
            checkerWithName.isStrictlyBetween(negative, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringP(), stringP()));
        }
    }

    @Test
    void isStrictlyBetweenThan_should_fail_if_strictly_less() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(negative);
        try {
            checker.isStrictlyBetween(zero, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_GREATER, stringZ(), stringN()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(negative).as(P_NAME);
        try {
            checkerWithName.isStrictlyBetween(zero, positive);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_GREATER, stringZ(), stringN()));
        }
    }

    @Test
    void isStrictlyBetweenThan_should_fail_if_strictly_greater() {
        AbstractFluentNumberChecker<C, A> checker = newChecker(positive);
        try {
            checker.isStrictlyBetween(negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_STRICTLY_LESS, stringZ(), stringP()));
        }

        AbstractFluentNumberChecker<C, A> checkerWithName = newChecker(positive).as(P_NAME);
        try {
            checkerWithName.isStrictlyBetween(negative, zero);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format(EXPECTED_P_STRICTLY_LESS, stringZ(), stringP()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected AbstractFluentNumberChecker<C, A> newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected abstract AbstractFluentNumberChecker<C, A> newChecker(A actual);

    @Override
    protected A newActual() {
        return numberZ();
    }

    /** Negative as number. */
    protected abstract A numberN();

    /** Zero as number. */
    protected abstract A numberZ();

    /** Positive as number. */
    protected abstract A numberP();

    /** Negative as string. */
    protected String stringN() {
        return numberN().toString();
    }

    /** Zero as string. */
    protected String stringZ() {
        return numberZ().toString();
    }

    /** Positive as string. */
    protected String stringP() {
        return numberP().toString();
    }
}
