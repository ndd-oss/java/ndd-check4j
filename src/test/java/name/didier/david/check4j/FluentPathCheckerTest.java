package name.didier.david.check4j;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import java.io.File;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

class FluentPathCheckerTest
        extends AbstractFluentCheckerTestBase<FluentPathChecker, Path> {

    private static final Path NOT_EXISTING = new File("some/path/that/is/really/unlikely/to/exist").toPath();
    private static final Path EXISTING_DIRECTORY = new File("src/test/resources").toPath();
    private static final Path EXISTING_FILE = new File("src/test/resources/logback-test.xml").toPath();

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void exists_should_return_self() {
        FluentPathChecker checkerD = newChecker(EXISTING_DIRECTORY);
        assertThat(checkerD.exists()).isEqualTo(checkerD);

        FluentPathChecker checkerF = newChecker(EXISTING_FILE);
        assertThat(checkerF.exists()).isEqualTo(checkerF);
    }

    @Test
    void exists_should_pass_if_path_exists() {
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).exists());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).as(P_NAME).exists());

        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).exists());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).as(P_NAME).exists());
    }

    @Test
    void exists_should_fail_if_path_does_not_exist() {
        FluentPathChecker checker1 = newChecker(NOT_EXISTING);
        try {
            checker1.exists();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing path");
        }

        FluentPathChecker checker2 = newChecker(NOT_EXISTING).as(P_NAME);
        try {
            checker2.exists();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing path");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isFile_should_return_self() {
        FluentPathChecker checker = newChecker(EXISTING_FILE);
        assertThat(checker.isFile()).isEqualTo(checker);
    }

    @Test
    void isFile_should_pass_if_path_is_a_regular_file() {
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).isFile());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_FILE).as(P_NAME).isFile());
    }

    @Test
    void isFile_should_fail_if_path_is_not_a_regular_file() {
        FluentPathChecker checker1 = newChecker(NOT_EXISTING);
        try {
            checker1.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular file");
        }

        FluentPathChecker checker2 = newChecker(NOT_EXISTING).as(P_NAME);
        try {
            checker2.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular file");
        }

        FluentPathChecker checker3 = newChecker(EXISTING_DIRECTORY);
        try {
            checker3.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular file");
        }

        FluentPathChecker checker4 = newChecker(EXISTING_DIRECTORY).as(P_NAME);
        try {
            checker4.isFile();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular file");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isDirectory_should_return_self() {
        FluentPathChecker checker = newChecker(EXISTING_DIRECTORY);
        assertThat(checker.isDirectory()).isEqualTo(checker);
    }

    @Test
    void isDirectory_should_pass_if_path_is_a_regular_directory() {
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).isDirectory());
        assertThatNoException().isThrownBy(
                () -> newChecker(EXISTING_DIRECTORY).as(P_NAME).isDirectory());
    }

    @Test
    void isDirectory_should_fail_if_path_is_not_a_regular_directory() {
        FluentPathChecker checker1 = newChecker(NOT_EXISTING);
        try {
            checker1.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular directory");
        }

        FluentPathChecker checker2 = newChecker(NOT_EXISTING).as(P_NAME);
        try {
            checker2.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular directory");
        }

        FluentPathChecker checker3 = newChecker(EXISTING_FILE);
        try {
            checker3.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to be an existing regular directory");
        }

        FluentPathChecker checker4 = newChecker(EXISTING_FILE).as(P_NAME);
        try {
            checker4.isDirectory();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to be an existing regular directory");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Path newActual() {
        return EXISTING_DIRECTORY;
    }

    @Override
    protected FluentPathChecker newChecker() {
        return new FluentPathChecker(newActual());
    }

    @Override
    protected FluentPathChecker newChecker(Path actual) {
        return new FluentPathChecker(actual);
    }
}
