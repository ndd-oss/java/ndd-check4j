package name.didier.david.check4j;

class ConciseIntegerCheckersTest
        extends AbstractConciseNumberCheckersTestBase<Integer> {

    @Override
    protected Integer checkNegative(final Integer numberA) {
        return ConciseCheckers.checkNegative(numberA);
    }

    @Override
    protected Integer checkNegative(final Integer numberA, final String pName) {
        return ConciseCheckers.checkNegative(numberA, pName);
    }

    @Override
    protected Integer checkStrictlyNegative(final Integer numberN) {
        return ConciseCheckers.checkStrictlyNegative(numberN);
    }

    @Override
    protected Integer checkStrictlyNegative(final Integer numberN, final String pName) {
        return ConciseCheckers.checkStrictlyNegative(numberN, pName);
    }

    @Override
    protected Integer checkPositive(final Integer numberN) {
        return ConciseCheckers.checkPositive(numberN);
    }

    @Override
    protected Integer checkPositive(final Integer numberN, final String pName) {
        return ConciseCheckers.checkPositive(numberN, pName);
    }

    @Override
    protected Integer checkStrictlyPositive(final Integer numberN) {
        return ConciseCheckers.checkStrictlyPositive(numberN);
    }

    @Override
    protected Integer checkStrictlyPositive(final Integer numberN, final String pName) {
        return ConciseCheckers.checkStrictlyPositive(numberN, pName);
    }

    @Override
    protected Integer checkLessThan(final Integer numberN, final Number max) {
        return ConciseCheckers.checkLessThan(numberN, max);
    }

    @Override
    protected Integer checkLessThan(final Integer numberN, final Number max, final String pName) {
        return ConciseCheckers.checkLessThan(numberN, max, pName);
    }

    @Override
    protected Integer checkStrictlyLessThan(final Integer numberN, final Number max) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max);
    }

    @Override
    protected Integer checkStrictlyLessThan(final Integer numberN, final Number max, final String pName) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max, pName);
    }

    @Override
    protected Integer checkGreaterThan(final Integer numberN, final Number min) {
        return ConciseCheckers.checkGreaterThan(numberN, min);
    }

    @Override
    protected Integer checkGreaterThan(final Integer numberN, final Number min, final String pName) {
        return ConciseCheckers.checkGreaterThan(numberN, min, pName);
    }

    @Override
    protected Integer checkStrictlyGreaterThan(final Integer numberN, final Number min) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min);
    }

    @Override
    protected Integer checkStrictlyGreaterThan(final Integer numberN, final Number min, final String pName) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min, pName);
    }

    @Override
    protected Integer checkBetween(final Integer numberN, final Number min, final Number max) {
        return ConciseCheckers.checkBetween(numberN, min, max);
    }

    @Override
    protected Integer checkBetween(final Integer numberN, final Number min, final Number max, final String pName) {
        return ConciseCheckers.checkBetween(numberN, min, max, pName);
    }

    @Override
    protected Integer checkStrictlyBetween(final Integer numberN, final Number min, final Number max) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max);
    }

    @Override
    protected Integer checkStrictlyBetween(final Integer numberN, final Number min, final Number max,
            final String pName) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max, pName);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Integer numberN() {
        return -1;
    }

    @Override
    protected Integer numberZ() {
        return 0;
    }

    @Override
    protected Integer numberP() {
        return 1;
    }

}
