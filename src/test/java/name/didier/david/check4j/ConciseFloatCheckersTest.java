package name.didier.david.check4j;

class ConciseFloatCheckersTest
        extends AbstractConciseNumberCheckersTestBase<Float> {

    @Override
    protected Float checkNegative(final Float numberA) {
        return ConciseCheckers.checkNegative(numberA);
    }

    @Override
    protected Float checkNegative(final Float numberA, final String pName) {
        return ConciseCheckers.checkNegative(numberA, pName);
    }

    @Override
    protected Float checkStrictlyNegative(final Float numberN) {
        return ConciseCheckers.checkStrictlyNegative(numberN);
    }

    @Override
    protected Float checkStrictlyNegative(final Float numberN, final String pName) {
        return ConciseCheckers.checkStrictlyNegative(numberN, pName);
    }

    @Override
    protected Float checkPositive(final Float numberN) {
        return ConciseCheckers.checkPositive(numberN);
    }

    @Override
    protected Float checkPositive(final Float numberN, final String pName) {
        return ConciseCheckers.checkPositive(numberN, pName);
    }

    @Override
    protected Float checkStrictlyPositive(final Float numberN) {
        return ConciseCheckers.checkStrictlyPositive(numberN);
    }

    @Override
    protected Float checkStrictlyPositive(final Float numberN, final String pName) {
        return ConciseCheckers.checkStrictlyPositive(numberN, pName);
    }

    @Override
    protected Float checkLessThan(final Float numberN, final Number max) {
        return ConciseCheckers.checkLessThan(numberN, max);
    }

    @Override
    protected Float checkLessThan(final Float numberN, final Number max, final String pName) {
        return ConciseCheckers.checkLessThan(numberN, max, pName);
    }

    @Override
    protected Float checkStrictlyLessThan(final Float numberN, final Number max) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max);
    }

    @Override
    protected Float checkStrictlyLessThan(final Float numberN, final Number max, final String pName) {
        return ConciseCheckers.checkStrictlyLessThan(numberN, max, pName);
    }

    @Override
    protected Float checkGreaterThan(final Float numberN, final Number min) {
        return ConciseCheckers.checkGreaterThan(numberN, min);
    }

    @Override
    protected Float checkGreaterThan(final Float numberN, final Number min, final String pName) {
        return ConciseCheckers.checkGreaterThan(numberN, min, pName);
    }

    @Override
    protected Float checkStrictlyGreaterThan(final Float numberN, final Number min) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min);
    }

    @Override
    protected Float checkStrictlyGreaterThan(final Float numberN, final Number min, final String pName) {
        return ConciseCheckers.checkStrictlyGreaterThan(numberN, min, pName);
    }

    @Override
    protected Float checkBetween(final Float numberN, final Number min, final Number max) {
        return ConciseCheckers.checkBetween(numberN, min, max);
    }

    @Override
    protected Float checkBetween(final Float numberN, final Number min, final Number max, final String pName) {
        return ConciseCheckers.checkBetween(numberN, min, max, pName);
    }

    @Override
    protected Float checkStrictlyBetween(final Float numberN, final Number min, final Number max) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max);
    }

    @Override
    protected Float checkStrictlyBetween(final Float numberN, final Number min, final Number max, final String pName) {
        return ConciseCheckers.checkStrictlyBetween(numberN, min, max, pName);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Float numberN() {
        return -1.0f;
    }

    @Override
    protected Float numberZ() {
        return 0.0f;
    }

    @Override
    protected Float numberP() {
        return 1.0f;
    }

}
