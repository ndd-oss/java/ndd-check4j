package name.didier.david.check4j;

class FluentFloatCheckerTest
        extends AbstractFluentNumberCheckerTestBase<FluentFloatChecker, Float> {

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected FluentFloatChecker newChecker(final Float actual) {
        return new FluentFloatChecker(actual);
    }

    @Override
    protected Float numberN() {
        return -1.0f;
    }

    @Override
    protected Float numberZ() {
        return 0.0f;
    }

    @Override
    protected Float numberP() {
        return 1.0f;
    }
}
