package name.didier.david.check4j;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

import org.junit.jupiter.api.Test;

class FluentStringCheckerTest
        extends AbstractFluentCharSequenceTestBase<FluentStringChecker, String> {

    private static final String STRING = "STRING";
    private static final String EMPTY = "";
    private static final String BLANK = "   ";

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isNotBlank_should_pass_if_not_blank() {
        assertThatNoException().isThrownBy(
                () -> newChecker().isNotBlank());
    }

    @Test
    void isNotBlank_should_return_self() {
        FluentStringChecker checker = newChecker();
        assertThat(checker.isNotBlank()).isSameAs(checker);
    }

    @Test
    void isNotBlank_should_fail_if_empty() {
        FluentStringChecker checker = newChecker(empty());
        try {
            checker.isNotBlank();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format("Expected parameter to not be blank but was <%s>", checker.getActual()));
        }
    }

    @Test
    void isNotBlank_should_fail_if_empty_with_pName() {
        FluentStringChecker checker = newChecker(empty()).as(P_NAME);
        try {
            checker.isNotBlank();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e)
                    .hasMessage(format("Expected parameter 'P' to not be blank but was <%s>", checker.getActual()));
        }
    }

    @Test
    void isNotBlank_should_fail_if_blank() {
        FluentStringChecker checker = newChecker(blank());
        try {
            checker.isNotBlank();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(format("Expected parameter to not be blank but was <%s>", checker.getActual()));
        }
    }

    @Test
    void isNotBlank_should_fail_if_blank_with_pName() {
        FluentStringChecker checker = newChecker(blank()).as(P_NAME);
        try {
            checker.isNotBlank();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e)
                    .hasMessage(format("Expected parameter 'P' to not be blank but was <%s>", checker.getActual()));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected String newActual() {
        return STRING;
    }

    @Override
    protected String blank() {
        return BLANK;
    }

    @Override
    protected String empty() {
        return EMPTY;
    }

    @Override
    protected FluentStringChecker newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected FluentStringChecker newChecker(final String actual) {
        return new FluentStringChecker(actual);
    }

    @Override
    protected Class<?> getActualOtherClass() {
        return StringBuffer.class;
    }
}
