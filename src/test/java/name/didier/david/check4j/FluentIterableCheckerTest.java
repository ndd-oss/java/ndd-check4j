package name.didier.david.check4j;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

class FluentIterableCheckerTest
        extends AbstractFluentCheckerTestBase<FluentIterableChecker<Iterable<Object>, Object>, Iterable<Object>> {

    // -----------------------------------------------------------------------------------------------------------------

    @Test
    void isNotEmpty_should_pass_if_not_empty() {
        assertThatNoException().isThrownBy(
                () -> newChecker().isNotEmpty());
    }

    @Test
    void isNotEmpty_should_return_self() {
        FluentIterableChecker<Iterable<Object>, Object> checker = newChecker();
        assertThat(checker.isNotEmpty()).isSameAs(checker);
    }

    @Test
    void isNotEmpty_result_should_not_be_casted() {
        List<String> stringList = newArrayList("some string");
        FluentIterableChecker<List<String>, String> checkerList = new FluentIterableChecker<>(stringList);
        List<String> uncastedStrings = checkerList.getActual();
        assertThat(uncastedStrings).isSameAs(stringList);

        Set<String> stringSet = newHashSet("some string");
        FluentIterableChecker<Set<String>, String> checkerSet = new FluentIterableChecker<>(stringSet);
        Set<String> uncastedStringSet = checkerSet.getActual();
        assertThat(uncastedStringSet).isSameAs(stringSet);
    }

    @Test
    void isNotEmpty_should_fail_if_null() {
        FluentIterableChecker<Iterable<Object>, Object> checker = newChecker(null);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_MESSAGE);
        }
    }

    @Test
    void isNotEmpty_should_fail_if_null_with_pName() {
        FluentIterableChecker<Iterable<Object>, Object> checker = newChecker(null).as(P_NAME);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage(NULL_P_MESSAGE);
        }
    }

    @Test
    void isNotEmpty_should_fail_if_empty() {
        FluentIterableChecker<Iterable<Object>, Object> checker = newChecker(newArrayList());
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter to not be empty");
        }
    }

    @Test
    void isNotEmpty_should_fail_if_empty_with_pName() {
        FluentIterableChecker<Iterable<Object>, Object> checker = newChecker(newArrayList()).as(P_NAME);
        try {
            checker.isNotEmpty();
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessage("Expected parameter 'P' to not be empty");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected Iterable<Object> newActual() {
        return newArrayList(new Object());
    }

    @Override
    protected FluentIterableChecker<Iterable<Object>, Object> newChecker() {
        return newChecker(newActual());
    }

    @Override
    protected FluentIterableChecker<Iterable<Object>, Object> newChecker(final Iterable<Object> actual) {
        return new FluentIterableChecker<>(actual);
    }
}
